/* ***************************************************************** */
/* File name:        seven_segments.c                                */
/* File description: Source file containing the functions/methods    */
/*                   interfaces for handling the seven segments      */
/*                   from the peripheral board                       */
/* Author name:      Daniel Gonzalez/Daniel Pedroso                  */
/* Creation date:    25mar2017                                       */
/* Revision date:    31mar2017                                       */
/* ***************************************************************** */

#include "seven_segments.h"

/* systems include */
#include "KL25Z/es670_peripheral_board.h"

/* ************************************************ */
/* Method name:        initSevenSegments            */
/* Method description: Initialize the seven segments*/
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void initSevenSegments ()
{
	//Ungate PORTC
	 SIM_SCGC5 = SIM_SCGC5_PORTC(CGC_CLOCK_ENABLED);

	 //Set as GPIO
	 PORTC_PCR0 = PORT_PCR_MUX(LS1_ALT);
	 PORTC_PCR1 = PORT_PCR_MUX(LS2_ALT);
	 PORTC_PCR2 = PORT_PCR_MUX(LS1_ALT);
	 PORTC_PCR3 = PORT_PCR_MUX(LS2_ALT);
	 PORTC_PCR4 = PORT_PCR_MUX(LS1_ALT);
	 PORTC_PCR5 = PORT_PCR_MUX(LS2_ALT);
	 PORTC_PCR6 = PORT_PCR_MUX(LS1_ALT);
	 PORTC_PCR7 = PORT_PCR_MUX(LS2_ALT);
	 PORTC_PCR13 = PORT_PCR_MUX(LS1_ALT);
	 PORTC_PCR12 = PORT_PCR_MUX(LS2_ALT);
	 PORTC_PCR11 = PORT_PCR_MUX(LS1_ALT);
	 PORTC_PCR10 = PORT_PCR_MUX(LS2_ALT);

	 //Define PORT as output
	 GPIOC_PDDR |= SEGA | SEGB | SEGC | SEGD | SEGE;
	 GPIOC_PDDR |= SEGF | SEGG | SEGP;
	 GPIOC_PDDR |=  SEGMENT4 | SEGMENT3 | SEGMENT2 | SEGMENT1;

}

/* ************************************************ */
/* Method name:        setSevenSegments             */
/* Method description: Set a value to the display   */
/* Input params:       char val = the desired       */
/*                                 character        */
/*                    char segment = the desired    */
/*                                   display        */
/* Output params:      n/a                          */
/* ************************************************ */
void setSevenSegments(char val, char segment)
{
	// Turn off the display
	GPIOC_PCOR |=  SEGA | SEGB | SEGC | SEGD | SEGE | SEGF | SEGG | SEGP;
	GPIOC_PCOR |= SEGMENT4 | SEGMENT3 | SEGMENT2 | SEGMENT1;

	switch(val)
	{

	case '0':
		GPIOC_PSOR |= SEGA | SEGB | SEGC | SEGD | SEGE | SEGF;
		break;

	case '1':
		GPIOC_PSOR |= SEGB | SEGC;
		break;

	case '2':
		GPIOC_PSOR |= SEGA | SEGB |  SEGD | SEGE | SEGG;
		break;

	case '3':
		GPIOC_PSOR |= SEGA | SEGB  | SEGC| SEGD  | SEGG;
		break;

	case '4':
		GPIOC_PSOR |=  SEGB | SEGC  | SEGF | SEGG ;
		break;

	case '5':
		GPIOC_PSOR |= SEGA | SEGC | SEGD  | SEGF | SEGG;
		break;

	case '6':
		GPIOC_PSOR |= SEGA | SEGC | SEGD | SEGE | SEGF | SEGG;
		break;

	case '7':
		GPIOC_PSOR |= SEGA | SEGB | SEGC ;
		break;

	case '8':
		GPIOC_PSOR |= SEGA | SEGB | SEGC | SEGD | SEGE | SEGF | SEGG;
		break;

	case '9':
		GPIOC_PSOR |= SEGA | SEGB | SEGC | SEGD | SEGF | SEGG;
		break;

	case 'a':
	case 'A':
		GPIOC_PSOR |= SEGA | SEGB | SEGC  | SEGE | SEGF | SEGG ;
		break;

	case 'b':
	case 'B':
		GPIOC_PSOR |=  SEGC | SEGD |SEGE | SEGF | SEGG ;
		break;

	case 'c':
	case 'C':
		GPIOC_PSOR |= SEGA  | SEGD | SEGE | SEGF  ;
		break;

	case 'd':
	case 'D':
		GPIOC_PSOR |=  SEGB | SEGC | SEGD | SEGE | SEGG;
		break;

	case 'e':
	case 'E':
		GPIOC_PSOR |= SEGA  | SEGD | SEGE | SEGF | SEGG;
		break;

	case 'f':
	case 'F':
		GPIOC_PSOR |= SEGA | SEGE | SEGF | SEGG;
		break;
	}

	switch (segment)
	{
	case '1':
		GPIOC_PSOR |=  SEGMENT1;
		break;

	case '2':
		GPIOC_PSOR |=  SEGMENT2;
		break;

	case '3':
		GPIOC_PSOR |=  SEGMENT3;
		break;

	case '4':
		GPIOC_PSOR |=  SEGMENT4;
		break;
	}
}
