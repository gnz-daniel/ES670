/* ***************************************************************** */
/* File name:        seven_segments.h                                */
/* File description: Header file containing the functions/methods    */
/*                   interfaces for handling the seven segments      */
/*                   from the peripheral board                       */
/* Author name:      Daniel Gonzalez/Daniel Pedroso                  */
/* Creation date:    25mar2017                                       */
/* Revision date:    31mar2017                                       */
/* ***************************************************************** */

#ifndef SOURCES_SEVEN_SEGMENTS_H_
#define SOURCES_SEVEN_SEGMENTS_H_




/* ************************************************ */
/* Method name:        initSevenSegments            */
/* Method description: Initialize the seven segments*/
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void initSevenSegments ();

/* ************************************************ */
/* Method name:        setSevenSegments             */
/* Method description: Set a value to the display   */
/* Input params:       char val = the desired       */
/*                                 character        */
/*                    char segment = the desired    */
/*                                   display        */
/* Output params:      n/a                          */
/* ************************************************ */
void setSevenSegments(char val, char segment);


#endif /* SOURCES_SEVEN_SEGMENTS_H_ */
