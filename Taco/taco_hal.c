/*
 * taco_hal.c
 *
 *  Created on: 18/05/2017
 *      Author: aluno
 */

#include "taco_hal.h"
#include "KL25Z/es670_peripheral_board.h"




/* ************************************************ */
/* Method name:        taco_initTaco                */
/* Method description: initialise tachometer        */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void taco_initTaco()
{
	SIM_SCGC5 |= SIM_SCGC5_PORTE(CGC_CLOCK_ENABLED); //IR ungate
	SIM_SCGC6 |= SIM_SCGC6_TPM0(CGC_TPM0_ENABLED);// TPM0 ungate

	PORTE_PCR29 |= PORT_PCR_MUX(IR_ALT4); // set IR as clock

	SIM_SOPT4 |= SIM_SOPT4_TPM0CLKSEL(SOPT4_CLKIN0_ENABLED);
	SIM_SOPT2 |= SIM_SOPT2_TPMSRC(SOPT2_OSCERCLK_ENABLED); //set OSCERCLK

	TPM0_SC   |= TPM_SC_CMOD(LPTM_EXTCLK_ENABLED); //set LPTM_EXTCLK
	TPM0_C2SC |= TPM_CnSC_ELSA(CnSC_RISINGEDGE_ENABLED); //rising edge
}



/* ************************************************ */
/* Method name:        taco_getCoolerTurns          */
/* Method description: get the number of turns      */
/* Input params:       n/a                          */
/* Outpu params:       number of turns              */
/* ************************************************ */
int taco_getCoolerTurns ()
{
	return TPM0_CNT/NUMBER_FANS;
}


/* ************************************************ */
/* Method name:        taco_resetCounter            */
/* Method description: reset counter                */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void taco_resetCounter ()
{
	TPM0_CNT = 0;
}
