/*
 * taco_hal.h
 *
 *  Created on: 18/05/2017
 *      Author: aluno
 */

#ifndef SOURCES_TACO_TACO_HAL_H_
#define SOURCES_TACO_TACO_HAL_H_

#define NUMBER_FANS      7

/* ************************************************ */
/* Method name:        taco_initTaco                */
/* Method description: initialise tachometer        */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void taco_initTaco();


/* ************************************************ */
/* Method name:        taco_getCoolerTurns          */
/* Method description: get the number of turns      */
/* Input params:       n/a                          */
/* Outpu params:       number of turns              */
/* ************************************************ */
int taco_getCoolerTurns ();


/* ************************************************ */
/* Method name:        taco_resetCounter            */
/* Method description: reset counter                */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void taco_resetCounter ();

#endif /* SOURCES_TACO_TACO_HAL_H_ */
