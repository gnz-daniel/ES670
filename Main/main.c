/* ***************************************************************** */
/* File name:        es670.c                                         */
/* File description: File dedicated to the ES670 prototype projects  */
/*                   involving the FRDM-KL25Z board together with is */
/*                   daughter board containing more peripherals      */
/*                                                                   */
/*                   Processor MKL25Z128VLK4 characteristics         */
/*                   48 MHz ARM Cortex-M0+ core                      */
/*                   128 KB program flash memory                     */
/*                   16 KB SRAM                                      */
/*                   Voltage range: 1.71 to 3.6 V                    */
/*                                                                   */
/* Author name:      dloubach                                        */
/* Creation date:    16out2015                                       */
/* Revision date:    09mai2017                                       */
/* ***************************************************************** */

/* our includes */
#include "KL25Z/es670_peripheral_board.h"
#include "Buzzer/buzzer_hal.h"
#include "Ledswi/ledswi_hal.h"
#include "seven_segments/seven_segments.h"
#include "Cooler/cooler_hal.h"
#include "Taco/taco_hal.h"
#include "Serial/serial_hal.h"
#include "Protocolo/cmdmachine_hal.h"
#include "Heater/heater_hal.h"
#include "ADC/adc_hal.h"
#include "lut_adc.h"
//#include "LedRgb/ledrgb_hal.h"
#include "Lcd/lcd_hal.h"
#include "util/mcg_hal.h"
#include "util/tc_hal.h"
#include "util/debugUart.h"
#include "Util/util.h"
#include "util/tc_hal.h"
#include "string.h"
/* system includes */
#include "fsl_debug_console.h"
#include "stdio.h"
#include "PID/pid_hal.h"



/* defines */
#define CYCLIC_EXECUTIVE_PERIOD         1000 * 300 /* 1000000 micro seconds */


/* globals */
volatile unsigned int uiFlagNextPeriod = 0;         /* cyclic executive flag */


/* ************************************************ */
/* Method name:        main_cyclicExecuteIsr        */
/* Method description: cyclic executive interrupt   */
/*                     service routine              */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void main_cyclicExecuteIsr(void)
{
    /* set the cyclic executive flag */
    uiFlagNextPeriod = 1;
}


/* ************************************************ */
/* Method name:        main_startLoop               */
/* Method description: displays the temperature on  */
/*                     LCD                          */
/* Input params:       n/a                          */
/* Output params:       n/a                         */
/* ************************************************ */
void main_startLoop()
{
	char cConst[32];
	char cValue[32];
	int sensor_value = taco_getCoolerTurns();
	int controller_output = pid_PidUpdate(sensor_value);
	if(controller_output < 0)
		controller_output = 0;
	if(UART0_BRD_S1_RDRF(UART0))
		cmdmachine_interpretCmd();
	buzzer_setBuzz();
	util_genDelay1ms();
	buzzer_clearBuzz();
	taco_resetCounter();
	cooler_setCooler(controller_output);
	//cooler_setCooler(1000);
	 // clear LCD
	 lcd_sendCommand(CMD_CLEAR);

	 // set the cursor line 0, column 0
	 lcd_setCursor(0,0);
	 // send string
	 sprintf(cConst,"P:%d I:%d D:%d",pid_getP(), pid_getI(), pid_getD());
	 lcd_writeString(cConst);
	 // set the cursor line 1, column 0
	 lcd_setCursor(1,0);
	 sprintf(cValue,"RPS:%d PWM:%d",sensor_value,controller_output);
	 lcd_writeString(cValue);



}

/* ************************************************ */
/* Method name:        main_boardInit               */
/* Method description: Initialize the peripherals   */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void main_boardInit()
{

	mcg_clockInit();
	buzzer_init();
	lcd_initLcd();
	cooler_initCooler();
	heater_initHeater();
	taco_initTaco();
	serial_setConfig();
	adc_initAdc();
	pid_initPID();
}





/* ************************************************ */
/* Method name:        main                         */
/* Method description: system entry point           */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
int main(void)
{
    /* board initializations */
	main_boardInit();
	double sensor_value;
	//struct PID_DATA pid_data;

    /* configure cyclic executive interruption */
    tc_installLptmr0(CYCLIC_EXECUTIVE_PERIOD, main_cyclicExecuteIsr);

    /* cooperative cyclic executive main loop */
    while(1U)
    {

     	main_startLoop();
    	/* WAIT FOR CYCLIC EXECUTIVE PERIOD */

        while(!uiFlagNextPeriod);
        uiFlagNextPeriod = 0;
    }

    /* Never leave main */
    return 0;
}

