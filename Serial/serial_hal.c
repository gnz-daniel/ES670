/*
 * serial_hal.c
 *
 *  Created on: 28/04/2017
 *      Author: aluno
 */
#include "print_scan.h"
#include "util/debugUart.h"
#include "serial_hal.h"
#include "fsl_debug_console.h"

void serial_setConfig ()
{
	debugUart_init();
}


void serial_sendBuffer(char c)
{
	PUTCHAR(c);
}

char serial_receiveBuffer()
{
	return GETCHAR();
}
