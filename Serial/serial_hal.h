/*
 * serial_hal.h
 *
 *  Created on: 28/04/2017
 *      Author: aluno
 */

#ifndef SOURCES_SERIAL_SERIAL_HAL_H_
#define SOURCES_SERIAL_SERIAL_HAL_H_

/* ************************************************ */
/* Method name:        serial_setConfig             */
/* Method description: Configure the serial         */
/*                      comunication                */
/* Input params:       n/a                          */
/* Output params:       n/a                         */
/* ************************************************ */
void serial_setConfig ();

/* ************************************************ */
/* Method name:        serial_sendBuffer            */
/* Method description: Write one character to the   */
/*                     serial.                      */
/* Input params:       char c: Character to send    */
/* Output params:       n/a                         */
/* ************************************************ */
void serial_sendBuffer(char c);

/* ************************************************ */
/* Method name:        serial_receiveBuffer         */
/* Method description: Read one character form the  */
/*                     serial.                      */
/* Input params:       n/a                          */
/* Output params:       Readed character            */
/* ************************************************ */
char serial_receiveBuffer();


#endif /* SOURCES_SERIAL_SERIAL_HAL_H_ */
