/*
 * adc_hal.c
 *
 *  Created on: 02/06/2017
 *      Author: aluno
 */
#include "adc_hal.h"
#include "KL25Z/es670_peripheral_board.h"

void adc_initAdc(){

	/* un-gate ports clock*/
	    SIM_SCGC5 |= SIM_SCGC5_PORTE(CGC_CLOCK_ENABLED);
	    SIM_SCGC6 |= SIM_SCGC6_ADC0(CGC_CLOCK_ENABLED);

	    // COLOCAR ALT0

	   ADC0_CFG1 |= ADC_CFG1_MODE(CFG1_MODE_8BIT); // MODE 8 BITS
}


void adc_startConvertion(){
	ADC0_SC1A = ADC_SC1_ADCH(SC1_ADCH_CHANNEL4);
}

int adc_isAdcDone()
{
	return  ((ADC0_SC1A & ADC_SC1_COCO_MASK) >> ADC_SC1_COCO_SHIFT);
}

unsigned int adc_getValue(){

	 adc_startConvertion();
	 while(adc_isAdcDone());
	 return ADC0_RA;
}
