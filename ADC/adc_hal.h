/*
 * adc_hal.h
 *
 *  Created on: 02/06/2017
 *      Author: aluno
 */

#ifndef SOURCES_ADC_ADC_HAL_H_
#define SOURCES_ADC_ADC_HAL_H_

void adc_initAdc();
unsigned int adc_getValue();
int adc_isAdcDone();
void adc_startConvertion();

#endif /* SOURCES_ADC_ADC_HAL_H_ */
