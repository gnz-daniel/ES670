/*
 * heater_hal.h
 *
 *  Created on: 02/06/2017
 *      Author: aluno
 */

#ifndef SOURCES_HEATER_HEATER_HAL_H_
#define SOURCES_HEATER_HEATER_HAL_H_


/* ************************************************ */
/* Method name:        cooler_initCooler            */
/* Method description: initialise cooler            */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void heater_initHeater();


/* ************************************************ */
/* Method name:        cooler_startCooler           */
/* Method description: set cooler speed             */
/* Input params:       uidutyCycle - Duty Cycle     */
/* Outpu params:       n/a                          */
/* ************************************************ */
void heater_setHeater(unsigned int uidutyCycle);

/* ************************************************ */
/* Method name:        cooler_stopCooler            */
/* Method description: stop cooler                  */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void heater_stopHeater();

#endif /* SOURCES_HEATER_HEATER_HAL_H_ */
