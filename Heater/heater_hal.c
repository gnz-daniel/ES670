/*
 * heater_hal.c
 *
 *  Created on: 02/06/2017
 *      Author: Daniel
 */

/*
 * cooler_hal.c
 *
 *  Created on: 18/05/2017
 *      Author: aluno
 */
#include "KL25Z/es670_peripheral_board.h"
#include "heater_hal.h"
#include "timer_counter.h"


/* ************************************************ */
/* Method name:        cooler_initCooler            */
/* Method description: initialise cooler            */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void heater_initHeater()
{
    /* un-gate port clock*/
    SIM_SCGC5 |= SIM_SCGC5_PORTA(CGC_CLOCK_ENABLED);

    /* set pin as PWM */
    PORTA_PCR12 |= PORT_PCR_MUX(0X03U);

	/* un-gate port clock*/
	SIM_SCGC6 |= SIM_SCGC6_TPM1(CGC_TPM1_ENABLED);; //Enable clock for TPM1

	TPM1_SC |= TPM_SC_CPWMS(LPTM_TPM1_UPCOUNT) ;   //up counting mode
	TPM1_SC |= TPM_SC_CMOD(1);   //LPTPM Counter increments on every LPTPM counter clock
	TPM1_SC |= TPM_SC_PS(0);   //Prescale 1:1

	TPM1_C0SC |= TPM_CnSC_ELSA(0) | TPM_CnSC_ELSB(1) | TPM_CnSC_MSA(0) | TPM_CnSC_MSB(1);
	TPM1_CNT = 0;
	TPM1_MOD = 1000;
	TPM1_C0V = 0; //
}


/* ************************************************ */
/* Method name:        cooler_startCooler           */
/* Method description: start cooler                 */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void heater_setHeater(unsigned int uidutyCycle)
{
	TPM1_C0V = uidutyCycle;
}


/* ************************************************ */
/* Method name:        cooler_stopCooler            */
/* Method description: stop cooler                  */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void heater_stopHeater()
{
	TPM1_C0V = 0;
}



