/*
 * pid_hal.h
 *
 *  Created on: 23/06/2017
 *      Author: aluno
 */

#ifndef SOURCES_PID_PID_HAL_H_
#define SOURCES_PID_PID_HAL_H_

struct PID_DATA{
	int Pgain, Dgain, Igain;
	int sensor_value_previous,reference_value;
	int error_sum;

};

/* ************************************************ */
/* Method name:        pid_initPID                  */
/* Method description: Initialize the controller    */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void pid_initPID();
/* ************************************************ */
/* Method name:        pid_pidUpdate                */
/* Method description: Update pid                   */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
int pid_PidUpdate(int sensor_value);
/* ************************************************ */
/* Method name:        pid_getP                     */
/* Method description: get P gain value             */
/* Input params:       n/a                          */
/* Output params:      Pgain                        */
/* ************************************************ */
int pid_getP();
/* ************************************************ */
/* Method name:        pid_getI                     */
/* Method description: get I gain value             */
/* Input params:       n/a                          */
/* Output params:      Igain                        */
/* ************************************************ */
int pid_getI();
/* ************************************************ */
/* Method name:        pid_getD                     */
/* Method description: get D gain value             */
/* Input params:       n/a                          */
/* Output params:      Dgain                        */
/* ************************************************ */
int pid_getD();
/* ************************************************ */
/* Method name:        pid_setP                     */
/* Method description: set P gain value             */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void pid_setP(int P);
/* ************************************************ */
/* Method name:        pid_setI                     */
/* Method description: get I gain value             */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void pid_setI(int I);
/* ************************************************ */
/* Method name:        pid_setD                     */
/* Method description: set D gain value             */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void pid_setD(int D);
/* ************************************************ */
/* Method name:        pid_setRef                   */
/* Method description: set ref value                */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
void pid_setRef(int ref);


#endif /* SOURCES_PID_PID_HAL_H_ */
