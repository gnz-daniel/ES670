/*
 * pid_hal.c
 *
 *  Created on: 23/06/2017
 *      Author: aluno
 */

#include "pid_hal.h"
static struct PID_DATA pid_data;
/* ************************************************ */
/* Method name:        pid_initPID                  */
/* Method description: Initialize the controller    */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */

void pid_initPID(){
	pid_data.Pgain = 35;
	pid_data.Igain = 10;
	pid_data.Dgain = 0;
	pid_data.sensor_value_previous = 0;
	pid_data.reference_value =  12;
}

/* ************************************************ */
/* Method name:        pid_pidUpdate                */
/* Method description: Update pid                   */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */
int pid_PidUpdate(int sensor_value)
{
	double Pterm, Iterm, Dterm;
	double error, error_previous, difference;
	error = pid_data.reference_value - sensor_value;
	error_previous = pid_data.reference_value - pid_data.sensor_value_previous;
	difference = error - error_previous;

	if(pid_data.Igain*pid_data.error_sum < 1000) // anti wind-up
		pid_data.error_sum += error;

	Pterm = pid_data.Pgain*error;
	Dterm = pid_data.Dgain*difference;
	Iterm = pid_data.Igain*pid_data.error_sum;

	pid_data.sensor_value_previous = sensor_value;
	if(Pterm+Iterm+Dterm > 1000)
		return 1000;
	else
		return (Pterm+Iterm+Dterm);
}

/* ************************************************ */
/* Method name:        pid_getP                     */
/* Method description: get P gain value             */
/* Input params:       n/a                          */
/* Output params:      Pgain                        */
/* ************************************************ */

int pid_getP(){
	return pid_data.Pgain;
}

/* ************************************************ */
/* Method name:        pid_getI                     */
/* Method description: get I gain value             */
/* Input params:       n/a                          */
/* Output params:      Igain                        */
/* ************************************************ */

int pid_getI(){
	return pid_data.Igain;
}

/* ************************************************ */
/* Method name:        pid_getD                     */
/* Method description: get D gain value             */
/* Input params:       n/a                          */
/* Output params:      Dgain                        */
/* ************************************************ */

int pid_getD(){
	return pid_data.Dgain;
}

/* ************************************************ */
/* Method name:        pid_setP                     */
/* Method description: set P gain value             */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */

void pid_setP(int P){
	pid_data.Pgain = P;
}

/* ************************************************ */
/* Method name:        pid_setI                     */
/* Method description: get I gain value             */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */

void pid_setI(int I){
	pid_data.Igain = I;
}

/* ************************************************ */
/* Method name:        pid_setD                     */
/* Method description: set D gain value             */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */

void pid_setD(int D){
	pid_data.Dgain = D;
}

/* ************************************************ */
/* Method name:        pid_setRef                   */
/* Method description: set ref value                */
/* Input params:       n/a                          */
/* Output params:      n/a                          */
/* ************************************************ */

void pid_setRef(int ref){
	pid_data.reference_value = ref;
	pid_data.error_sum = 0;
	pid_data.sensor_value_previous = ref;
}



