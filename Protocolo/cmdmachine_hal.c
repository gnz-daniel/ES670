#include "Ledswi/ledswi_hal.h"
#include "Buzzer/buzzer_hal.h"
#include "util/util.h"
#include "Serial/serial_hal.h"
#include "Cooler/cooler_hal.h"
#include "PID/pid_hal.h"

#define SEND_ACK	serial_sendBuffer('A');serial_sendBuffer('C');serial_sendBuffer('K');serial_sendBuffer(' ')
#define SEND_ERR	serial_sendBuffer('E');serial_sendBuffer('R');serial_sendBuffer('R');serial_sendBuffer(' ')

/* ************************************************ */
/* Method name:        ledCmd                       */
/* Method description: Check buffer and act on LED  */
/*                      accordingly                 */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_ledCmd(){
	char status = serial_receiveBuffer();
	unsigned int uiLed = serial_receiveBuffer() - '0';
	if(uiLed <= MAX_LED_SWI && uiLed >0){
		switch(status)
		{
		case 'C':
		case 'c':
			SEND_ACK;
			ledswi_clearLed(uiLed);
			break;
		case 'S':
		case 's':
			SEND_ACK;
			ledswi_setLed(uiLed);
			break;
		case 'V':
		case 'v':
			SEND_ACK;
			if(ledswi_getSwitchStatus(uiLed))
				serial_sendBuffer('c');
			else
				serial_sendBuffer('o');




		}
	}else{
		SEND_ERR;
	}
}
/* ************************************************ */
/* Method name:        swCmd                        */
/* Method description: Check switch status          */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */

void cmdmachine_swCmd(){
	unsigned int uiSw = serial_receiveBuffer() - '0';
	if(uiSw <= MAX_LED_SWI && uiSw > 0){
		SEND_ACK;
		if(ledswi_getSwitchStatus(uiSw))
			serial_sendBuffer('c');
		else
			serial_sendBuffer('o');
	}
	else
	{
		SEND_ERR;
	}

}
/* ************************************************ */
/* Method name:        buzzerCmd                    */
/* Method description: Set Buzzer for a period of   */
/*                     time                         */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_buzzerCmd(){
	unsigned int uiCent = serial_receiveBuffer() - '0';
	unsigned int uiDez = serial_receiveBuffer() - '0';
	unsigned int uiUni = serial_receiveBuffer() - '0';
	if(uiCent < 10 && uiDez < 10 && uiUni <10){
		unsigned int uiCycle = 100*uiCent + 10*uiDez + uiUni;
		unsigned int uiCont = 0;
		SEND_ACK;

		while(uiCont < uiCycle*2){
			 buzzer_setBuzz();
			 util_genDelay250us();
			 buzzer_clearBuzz();
			 util_genDelay250us();
			 uiCont ++;
		}

	}
	else{
		SEND_ERR;
	}
}
/* ************************************************ */
/* Method name:        pwmCmd                       */
/* Method description: Set the pwm duty cycle       */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_pwmCmd()
{
	unsigned int uiCent = serial_receiveBuffer() - '0';
	unsigned int uiDez = serial_receiveBuffer() - '0';
	unsigned int uiUni = serial_receiveBuffer() - '0';

	if(uiCent < 10 && uiDez < 10 && uiUni <10)
	{
			unsigned int uidutyCycle = 100*uiCent + 10*uiDez + uiUni;
			SEND_ACK;
			cooler_setCooler(uidutyCycle * 10);
	}
}

void cmdmachine_pid(){
	char cReceiveBuff = serial_receiveBuffer();
	unsigned int uiDez = serial_receiveBuffer() - '0';
	unsigned int uiUni = serial_receiveBuffer() - '0';
	int iValue = 10*uiDez + uiUni;
	switch(cReceiveBuff)
	{
	case 'P':
	case 'p':
		pid_setP(iValue);
		break;
	case 'I':
	case 'i':
		pid_setI(iValue);
		break;
	case 'D':
	case 'd':
		pid_setD(iValue);
		break;
	case 'R':
	case 'r':
		pid_setRef(iValue);
		break;
	default:
		SEND_ERR;
	}
}

/* ************************************************ */
/* Method name:        interpretCmd                 */
/* Method description: Find which device to act     */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_interpretCmd()
{
	char cReceiveBuff = serial_receiveBuffer();
	switch(cReceiveBuff)
	{
	case 'L':
	case 'l':
		SEND_ACK;
		cmdmachine_ledCmd();
		break;
	case 'B':
	case 'b':
		SEND_ACK;
		cmdmachine_buzzerCmd();
		break;
	case 'S':
	case 's':
		SEND_ACK;
		cmdmachine_swCmd();
		break;
	case 'M' :
	case 'm' :
		SEND_ACK;
		cmdmachine_pwmCmd();
		break;
	case 'K':
	case 'k':
		SEND_ACK;
		cmdmachine_pid();
		break;
	default:
		SEND_ERR;

	}
}

