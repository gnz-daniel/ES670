#ifndef __cmdmachine_h__
#define __cmdmachine_h__


/* ************************************************ */
/* Method name:        swCmd                        */
/* Method description: Check switch status          */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_swCmd();


/* ************************************************ */
/* Method name:        ledCmd                       */
/* Method description: Check buffer and act on LED  */
/*                      accordingly                 */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_ledCmd();


/* ************************************************ */
/* Method name:        buzzerCmd                    */
/* Method description: Set Buzzer for a period of   */
/*                     time                         */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_buzzerCmd();

/* ************************************************ */
/* Method name:        pwmCmd                       */
/* Method description: Set the pwm duty cycle       */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_pwmCmd();

/* ************************************************ */
/* Method name:        interpretCmd                 */
/* Method description: Find which device to act     */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cmdmachine_interpretCmd();

void cmdmachine_pid();
#endif
