/*
 * cooler_hal.h
 *
 *  Created on: 18/05/2017
 *      Author: aluno
 */




#ifndef SOURCES_COOLER_COOLER_HAL_H_

#define SOURCES_COOLER_COOLER_HAL_H_


/* ************************************************ */
/* Method name:        cooler_initCooler            */
/* Method description: initialise cooler            */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cooler_initCooler();


/* ************************************************ */
/* Method name:        cooler_startCooler           */
/* Method description: set cooler speed             */
/* Input params:       uidutyCycle - Duty Cycle     */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cooler_setCooler(unsigned int uidutyCycle);

/* ************************************************ */
/* Method name:        cooler_stopCooler            */
/* Method description: stop cooler                  */
/* Input params:       n/a                          */
/* Outpu params:       n/a                          */
/* ************************************************ */
void cooler_stopCooler();



#endif /* SOURCES_COOLER_COOLER_HAL_H_ */
